# Data Painting Mother

Fundamentals for data paintings with Python, Pandas, Flask, p5.js

## important parameters
* size of particles pyhsicial
* gravity strength
* opacity

## Quickstart

start with `pipenv shell` and `./main.py`

- `a` - add new particle
- `r` - delete particle
- `shift+click` - define polygon
- `P` - print list of polygons and reset

### possible animations

- breathing expand
- particles in between
- burst to pieces
- painting strokes
- wind
- colorchange
- shadow
- gravity change
- burst
- several layers

## Future dev

- fastapi instead of flask?

# The Frame TV Samsung

use the Internet browser and disable the always visible menu bar in the settings to enable fullscreen display.

# Record Canvas

with browser extension: https://chrome.google.com/webstore/detail/canvas-capture/pnhaaddlgbpchligciolcdjgndcpelee 


# from the latest dev of the CO² canvas


## used classes
* **Impediments**: Physical and attractive objects as obstacles for the particles. Their postion is static. They have no sprites but use the canvas.
* **Particle**: Physical and attractive objects representing CO² emissions. Randomly chosen out of 10 sprites.
* **Origin** - The place where the particles are dropped. They also hold the rate of increase of CO² and the connection to the dataset.
* **editor** - helps with defining the objects and their vertices

There is a deubg mode for seeing additional stats and visualizin the physical bodies. The impediment only switch shows just the layer of impediments in order to define the vertices.

## Resizing

The canvas responsively resizes to the width & height (`Window.innerHeight` & `Window.innerHeight`). In p5.js the resizing is done by scaling the drawn images with respect to new width or height. Matter.js is more difficult since the objects can only be changed relative to their current position and not positioned at an absolute location. For static objects: the `scale()` function is used for recalculating the object's size and its physics + `translate)()` to move the object to the new location after rescaling the whole scene. The difference from acutal position at actual size to same location to new size. The  non-static objects are removed and created newly after resizing.


## defining the coordinates of brush sprites
* enable rulers in Krita

## Performance know how

comes from: https://github.com/processing/p5.js/wiki/Optimizing-p5.js-Code-for-Performance

## guide for minting

https://github.com/hicetnunc2000/hicetnunc/wiki/Interactive-OBJKTs

# p5.js and Flask

Example for flask + p5.js comes from: [https://medium.com/@swaroopkml96/deploying-your-p5-js-sketch-using-flask-on-heroku-8702492047f5](mailto:https://medium.com/@swaroopkml96/deploying-your-p5-js-sketch-using-flask-on-heroku-8702492047f5)

- added the latest p5.js version number
- start the server locally with `python main.py` or with `./main.py` after adding the Shebang line `#!/usr/bin/env python`

start the productive server with: `gunicorn wsgi:app`

## p5.js and data via API

using the insights from <https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask> in combination with <https://p5js.org/reference/#/p5/httpGet>. The preload does not wait until the data is fetched (like in the example of httpGet).

dealt with CORS using `flask_cors` and the infos from: <https://flask-cors.readthedocs.io/en/latest/>

## p5.js and matter.js

world, engine - renderer is done by p5.js. create the element in physics world (matter.js) first and then let it draw by p5.js. `physical_body` and `attractive_shape`

use `poly-decomp` for matter.js to understand concave vertices: <https://github.com/schteppe/poly-decomp.js/> - code saved to `static folder` since CDN did not work.

You can use the angle value of the physical body (matter.js world) for rotating the attractive shape of p5.js, with translate und rotate(angle) - e.g.
 https://editor.p5js.org/aryan.chharia@gmail.com/sketches/q0ITOza9s 

### inspirational examples
* https://brm.io/matter-js/demo/#timescale
* https://brm.io/matter-js/demo/#avalanche 
* https://brm.io/matter-js/demo/#terrain 
* https://liabru.github.io/matter-attractors/#basic (attractors plugin)
* https://liabru.github.io/matter-attractors/#gravity (attractors plugin)

### attractors plugin

installing the attractor plugin via cdn and add main literature: <https://github.com/liabru/matter-attractors> ... `bodyA` is the body itself, `bodyB` each other body in the world. I can't control the attraction force. an object is either static or is attracted.

```html
<script src="https://cdn.jsdelivr.net/npm/matter-attractors@0.1.6/build/matter-attractors.min.js"
  integrity="sha256-XVi6kQJNjdbjQyRNAIuyfR2VB/jViITCG2pt5HvW2NQ=" crossorigin="anonymous"></script>
```

```javascript
//  declaration
Matter.use('matter-attractors');
let attractor;


//setup
 // add attractor
  attractor = Bodies.circle(400, 400, 50, {
    isStatic: true,
  });
  World.add(engine.world, attractor);


 // draw attractor
  push();
  fill(130, 50);
  beginShape();
  for (var i = 0; i < attractor.vertices.length; i++) {
    vertex(attractor.vertices[i].x, attractor.vertices[i].y);
  }
  endShape();
  pop();


  attractor.plugin.attractors = [
    function (bodyA, bodyB) {
      return {
        x: (bodyA.position.x - bodyB.position.x) * 1e-6,
        y: (bodyA.position.y - bodyB.position.y) * 1e-6,
      };
    }
  ]


```

## Positioning

matter.js uses `centre of mass` for positioning. p5.js uses top left. After creating the matter body, which changes the coordinates of the vertices slightly, I calculate the centre: `centre of mass` used for positioning. There is the image of rectangular form with the attractive shape on it - the sprite is a rectangular png with positioning. the physical_body has a mouse constraint and needs an offset: centre of mass versus top left.

with `Bodies.fromVertices` (like in the data_painting project) function the position was off a few pixel, the same data with `Body.create()` positioned the centre of mass exactly at the given position. Learning from: <https://stackoverflow.com/questions/30675883/how-should-i-place-an-object-using-matter-object-create-in-matter-js>

to prevent rotation in matter.js when moving the object, `inertia` can be set to `Infinity` (<https://stackoverflow.com/questions/36477294/matter-js-body-without-rotation-around-its-center>)

### how to position static elements with a sprite
* turn on the switch to only see the impediment layer.
* export a sprite png in krita.
* create an entry in vertices_data.js with sample data. dummy position at 300,3000 and reference to the image file. with *isStatic: true*
* include the sprite in sketch.js with preloading the image and initiating the object as a Particle
* get the coordinate of the left top of the rectangle in the console by shift+left_mouse in order to position the sprite. add it then as position coordinates to vertices_data.js. you can replace the physical body for now with the pyhsical offset if it is in the way.
* draw the vertices of the sprite by using Shift+left_mouse and press "P". copy the vertices from the console to vertices_data.js
* after the vertices of the physical body resemble those of the attractive body, get the offset between those two in order to align them. get the coordinate of the approximate centre of mass of the correctly positioned attractive shape with shift+left_mouse. 
* then calculate the `offsetPhysical` with: position.x - approximateCentre.x (as well as for y) and write it to vertices_data.js
* then fine tune the result.

### no svg

Reading svg in p5.js sucks - complicated, external scripts and errors, using inkscape for editing polygons sucks as well. So I skipped it and created a custom editor for creating polygons.

In the old fashioned way, I used inkscape to draw the svg bodies save them and had to parse the xml data. this lead to errors in understanding the `d` line.

## Animation

- Example for rotating around centre: <https://editor.p5js.org/faithyu/sketches/S14D5MSUb> and <https://stackoverflow.com/questions/52484101/move-along-an-arc-in-p5js>
- Example for moving with conditions: <https://editor.p5js.org/sektionschef/sketches/cmkzbQCB6>

## Editor

Goal: draw polygons, align attractive shape and physical body for offset and move both to a specific position.

learning from examples

- is point in attractive polygon example and function: <https://editor.p5js.org/makio135/sketches/O9xQNN6Q> (nice for drawing vertices with the mouse)
- drag object with mouse: <https://editor.p5js.org/enickles/sketches/H1n19TObz>
- matter.js and mouseconstraint: <https://www.youtube.com/watch?v=W-ou_sVlTWk> (with a mouseconstraint object, linking p5.js canvas and matter.js with `elt` element, and check for retina display with pixeldensity function)

mouseIsPressed and keyIsPressed and keyIsDown are events but they fire so often. so I use the functions mousePressed an keyPressed.

I think I have to match attractive shape with the physical body and save the offset in the json. and in a second step translate the physical body to the attractive shape.

## logging

loglevel is used (<https://pimterry.github.io/loglevel/>) for getting different log levels. there is a conflict with some global variable. the documentation proposed to add this:

```javascript
<script src="loglevel.min.js"></script>
<script>
var logging = log.noConflict();

logging.warn("still pretty easy");
</script>
```

## Gcloud

adding pipfile and pipfile.lock to .gcloudignore since it is not supported by gcp app engine. made a static requirements.txt with `pip freeze > requirements.txt`. <https://stackoverflow.com/questions/58546089/does-google-app-engine-flex-support-pipfile>

https://datapaintingmother.nw.r.appspot.com/ 

### quickstart

deploy mit in der pipenv: `gcloud app deploy app.yaml`
und aufrufen mit: `gcloud app browse`
einloggen mit: `gcloud auth login`

### Added Google Cloud Repo Mirror

using this site (<https://cloud.google.com/architecture/mirroring-gitlab-repositories-to-cloud-source-repositories>)

