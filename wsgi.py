from main import app
from config import ENV_VAR

if __name__ == "__main__":
    app.run(host="localhost", port=ENV_VAR['port'])
