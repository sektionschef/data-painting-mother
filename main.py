#!/usr/bin/env python
from flask import Flask, render_template, url_for, send_from_directory, jsonify
from flask_bootstrap import Bootstrap
from flask_cors import CORS
import logging

from config import ENV_VAR


# logging.getLogger('flask_cors').level = logging.DEBUG
# logging.debug('This message should go to the log file')

app = Flask(__name__)
bootstrap = Bootstrap(app)
# for all
CORS(app)
#  for dev PORT
# cors = CORS(app, resources={r"/api/*": {"origins": "http://127.0.0.1:5000"}})


@app.route("/")
def home():
    return render_template("home.html")


@app.route("/static/<path:path>")
def send_static(path):
    return send_from_directory("static", path)


# API
tasks = [
    {
        "id": 1,
        "title": u"Buy groceries",
        "description": u"Milk, Cheese, Pizza, Fruit, Tylenol",
        "done": False,
    },
    {
        "id": 2,
        "title": u"Learn Python",
        "description": u"Need to find a good Python tutorial on the web",
        "done": False,
    },
]


@app.route("/api/v1.0/tasks", methods=["GET"])
def get_tasks():

    return jsonify({"tasks": tasks})


if __name__ == "__main__":
    app.run(host="localhost", port=ENV_VAR["port"], debug=True)
